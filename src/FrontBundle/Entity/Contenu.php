<?php

namespace FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contenu
 *
 * @ORM\Table(name="contenu")
 * @ORM\Entity(repositoryClass="FrontBundle\Repository\ContenuRepository")
 */
class Contenu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="post1", type="string", length=255)
     */
    private $post1;

    /**
     * @var string
     *
     * @ORM\Column(name="post2", type="string", length=255)
     */
    private $post2;

    /**
     * @var int
     *
     * @ORM\Column(name="nbpost", type="integer")
     */
    private $nbpost;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set post1
     *
     * @param string $post1
     *
     * @return Contenu
     */
    public function setPost1($post1)
    {
        $this->post1 = $post1;

        return $this;
    }

    /**
     * Get post1
     *
     * @return string
     */
    public function getPost1()
    {
        return $this->post1;
    }

    /**
     * Set post2
     *
     * @param string $post2
     *
     * @return Contenu
     */
    public function setPost2($post2)
    {
        $this->post2 = $post2;

        return $this;
    }

    /**
     * Get post2
     *
     * @return string
     */
    public function getPost2()
    {
        return $this->post2;
    }

    /**
     * Set nbpost
     *
     * @param integer $nbpost
     *
     * @return Contenu
     */
    public function setNbpost($nbpost)
    {
        $this->nbpost = $nbpost;

        return $this;
    }

    /**
     * Get nbpost
     *
     * @return int
     */
    public function getNbpost()
    {
        return $this->nbpost;
    }
}

